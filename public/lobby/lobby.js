window.onload = () => {
  const activePlayers = [];
  const players = document.querySelector('#players');
  const socket = io.connect('http://localhost:3000');
  const jwt = localStorage.getItem('jwt');
  const logout = document.querySelector('#logout');

  if(!jwt) {
    location.replace('/login');
  } else {
      logout.addEventListener('click', ev => {
        if(localStorage.getItem('jwt')){
          localStorage.removeItem('jwt');
          socket.emit('playerHasLeft', { token: jwt });
        } location.replace('/');
      });

      socket.emit('playerEntered', { token: jwt });

      socket.on('newPlayerJoined', payload => {
        payload.players.forEach(player => {
         if (activePlayers.indexOf(player) === -1) {
           activePlayers.push(player);
           addPlayer(player);
         }
       });
      });

      socket.on('raceStarting', () => {
        location.replace('/race');
      });

      socket.on('timeOut', payload => {
        const { timeOut } = payload;
        startTimer(timeOut, document.querySelector('#time'));
      });

      socket.on('playerHasLeftBroadcast', payload => {
        const player = document.querySelector(`#${payload.player.toString().replace(/ /g, '_')}`);
        if (activePlayers.has(payload.player)) {
          activePlayers.delete(payload.player);
          players.removeChild(player);
        }
      })
    }

  function addPlayer(player) {
    const div = document.createElement('div');
    div.classList.add('player');
    const span = document.createElement('span');
    span.innerHTML = player;
    div.appendChild(span);
    const progress = document.createElement('progress');
    progress.setAttribute('id' , player.toString().replace(/ /g, '_'));
    progress.setAttribute('value' , `${0}`);
    progress.setAttribute('max' , `${300}`);
    div.appendChild(progress);
    players.appendChild(div);
  }

  function startTimer (duration, display) {
    let timer = duration, minutes, seconds;
    setInterval(function () {
      minutes = parseInt(timer / 60, 10);
      seconds = parseInt(timer % 60, 10);

      minutes = minutes < 10 ? "0" + minutes : minutes;
      seconds = seconds < 10 ? "0" + seconds : seconds;

      display.textContent = `${minutes}:${seconds}`;

      if (--timer < 0) {
        timeOut = 0;
        socket.emit('timeoutEnd', { token: jwt });
      }
    }, 1000);
  }

};
