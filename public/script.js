window.onload = () => {
  var text = document.querySelector('#textArea');
  var sendButton = document.querySelector('#sendButton');
  var messagesList = document.querySelector('#messagesList');

  const socket = io.connect('http://localhost:3000');

  sendButton.addEventListener('click', ev => {
    socket.emit('submitMessage', { message: text.value });
  });

  socket.on('newMessage', payload => {
   const newLi =  document.createElement('li');
    newLi.innerHTML = payload.message;
    messagesList.appendChild(newLi);
  })

};
