window.onload = () => {
  const text = document.querySelector('#textArea');
  const sendButton = document.querySelector('#sendButton');
  const messagesList = document.querySelector('#messagesList');
  const logOut = document.querySelector('#logOut');
  const socket = io.connect('http://localhost:3000');
  const jwt = localStorage.getItem('jwt');
  const date = new Date();

  if(!jwt) {
    location.replace('/login');
  } else {
    sendButton.addEventListener('click', ev => {
      socket.emit('submitMessage', { message: text.value, token: jwt });
    });

    logOut.addEventListener('click', ev => {
      location.replace('/logout')
    });

    socket.on('newMessage', payload => {
      const newLi =  document.createElement('li');
      newLi.innerHTML = `${payload.userLogin}: (${date.getHours()}:${date.getMinutes()}) ${payload.message}`;
      messagesList.appendChild(newLi);
    })
  }
};
