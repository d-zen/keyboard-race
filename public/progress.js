function runProgress() {
  var progress = document.getElementById('progress'),
    percent = document.getElementById('percent'),
    done = document.getElementById('done');

  if (!progress.value)
    progress.value = +progress.getAttribute("value");
  if (!progress.max)
    progress.max = +progress.getAttribute("max");

  function changeProgress() {
    if (progress.value >= progress.max) {
      done.innerHTML = "Готово!";
      return;
    }
    progress.value++;
    percent.innerHTML = Math.floor(progress.value / progress.max * 100);
    setTimeout(changeProgress, 10);
  }

  changeProgress();
}
