window.onload = () => {
  const jwt = localStorage.getItem('jwt');
  if (jwt) {
    location.replace('/lobby');
  } else {
    location.replace('/login');
  }
};