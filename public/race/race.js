window.onload = () => {
  const socket = io.connect('http://localhost:3000');
  const trackHTML = document.querySelector('#track');
  const logOut = document.querySelector('#logOut');
  const commentator = document.querySelector('#commentator');
  const jwt = localStorage.getItem('jwt');
  const textSymbols =[];
  const spanValues =[];
  let currentPosition = 0;
  let correct = 0;

  if (!jwt) {
    location.replace('/login');
  } else {
    logOut.addEventListener('click', ev => {
      localStorage.removeItem('jwt');
      location.replace('/login')
    });

    socket.emit('playerEnteredRace', { token: jwt });

    socket.on('raceStarted', payload => {
      const { player, otherPlayers, track, time } = payload;
      otherPlayers.forEach(player => {
        addPlayer(player, track);
      });

      startTimer(time, document.querySelector('#time'));

      track.split('').forEach(el => {
        textSymbols.push(el.toLowerCase());
        addSymbol(el);
      });

      Array.from(trackHTML.getElementsByTagName('span')).forEach(el => {
        spanValues.push(el)
      });
      spanValues[0].className = 'current';
      // console.log(`position: ${currentPosition}/ current letter: ${textSymbols[currentPosition]} / correct: ${correct}`);

      document.addEventListener('keypress', (ev) => {
        currentPosition++;
        spanValues[currentPosition].className = 'current';
        if (currentPosition) {
          spanValues[(currentPosition - 1)].className = 'previous';
        }
        if (ev.key.toLowerCase() === textSymbols[currentPosition-1]) {
          correct++;
        }


        socket.emit('playerProgressed', { token: jwt });

        console.log(`position: ${currentPosition}/ current letter: ${textSymbols[currentPosition]} / pressed: ${ev.key.toLowerCase()} / correct: ${correct}`);
      });

    });

    socket.on('progressSubmitted', payload => {
      addProgress(payload.player);
    });

    socket.on('finishSubmitted', payload => {
      location.replace('/lobby');
    });

    socket.on('commented', payload => {
      publishComment(payload.comment);
    })

  }

  function publishComment(comment) {
    commentator.innerText = comment;
  }

  function addPlayer(player, track) {
    const div = document.createElement('div');
    div.classList.add('player');
    const span = document.createElement('span');
    span.innerHTML = player;
    div.appendChild(span);
    const progress = document.createElement('progress');
    progress.setAttribute('id' , player.toString().replace(/ /g, '_'));
    progress.setAttribute('value' , `${0}`);
    progress.setAttribute('max' , `${track.length}`);
    div.appendChild(progress);
    players.appendChild(div);
  }

  function addSymbol(symbol) {
    const span = document.createElement('span');
    span.textContent = symbol;
    trackHTML.appendChild(span);
  }

  function startTimer (duration, display) {
    let timer = duration, minutes, seconds;
    setInterval(function () {
      minutes = parseInt(timer / 60, 10);
      seconds = parseInt(timer % 60, 10);

      minutes = minutes < 10 ? "0" + minutes : minutes;
      seconds = seconds < 10 ? "0" + seconds : seconds;

      display.textContent = `${minutes}:${seconds}`;

      if (--timer < 0) {
        timeOut = 0;
        socket.emit('raceTimeEnd', { token: jwt });
        location.replace('/lobby');
      }
    }, 1000);
  }

  function addProgress(player) {
    const progress = document.querySelector(`#${player}`);
      if (progress.value >= progress.max) {
        socket.emit('playerFinished', { token: jwt });
        return;
      }
      progress.value++;
  }
};
