window.onload = () => {

  const loginField = document.querySelector('#login');
  const password = document.querySelector('#password');
  const loginBtn = document.querySelector('#loginBtn');

  loginBtn.addEventListener('click', ev => {
    fetch('/login', {
      method: 'POST',
      headers: {
        'Content-Type' : 'application/json'
      },
      body: JSON.stringify({
        login: loginField.value,
        password: password.value
      })
    }).then(res => {
      res.json().then(body => {
        console.log(body);
        if(body.auth) {
          localStorage.setItem('jwt', body.token);
          location.replace('/lobby');
        } else {
          console.log('auth failed');
        }
      })
    }).catch(err => {
      console.log('Request failed. ', err);
    })
  });

};