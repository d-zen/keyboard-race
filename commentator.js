class Commentator {

  constructor(name) {
    this.name = name;
  }

  emitComment(eventType, data) {
    const { racers } = data;
      return new Comment(eventType, racers);
    }

}
