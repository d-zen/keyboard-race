const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./users.json');
const tracks = require('./tracks.json');
const commentators = require('./commentators.json');
const comments = require('./comments.json');
require('./passport.config.js');

const activePlayers = [];
let playersProgress = {};
let commentator;
let refreshIntervalId;

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

server.listen(3000);

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/login', function (req, res) {
  res.sendFile(path.join(__dirname, './public/login/login.html'));
});

app.get('/logout', /*passport.authenticate('jwt'),*/ function (req, res) {
  if(localStorage.getItem('jwt')){
    localStorage.removeItem('jwt');
  } else {
    localStorage.clear()
  }
  res.sendFile(path.join(__dirname, 'index.html'));
});


app.get('/chat', /*passport.authenticate('jwt'),*/ function (req, res) {
  res.sendFile(path.join(__dirname, './public/chat/chat.html'));
});

app.get('/lobby', /*passport.authenticate('jwt'),*/ function (req, res) {
  res.sendFile(path.join(__dirname, './public/lobby/lobby.html'));
});

app.get('/race', /*passport.authenticate('jwt'),*/ function (req, res) {
  res.sendFile(path.join(__dirname, './public/race/race.html'));
});

app.post('/login', (req, res) => {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret', {expiresIn: '24h' });
    res.status(200).json({auth: true, token});
  } else {
    res.status(401).json({auth: false});
  }

});

// app.use(express.static(path.join(__dirname, 'public')));

io.on('connection', socket => {
  socket.on('submitMessage', payload => {
    const { message, token } = payload;
    const userLogin = jwt.decode(token).login; // todo: check if user data is valid && key still in one peace and intact
    socket.broadcast.emit('newMessage', { message, userLogin });
    socket.emit('newMessage', { message, userLogin });
  });

  socket.on('playerEnteredRace', payload => {
    const userLogin = jwt.decode(payload.token).login;
    if (activePlayers.indexOf(userLogin) === -1) {
      activePlayers.push(userLogin);
    }
    // race = new Race(activePlayers, tracks[1]);
    socket.emit('raceStarted', { player: userLogin, otherPlayers: activePlayers, track: tracks[2].text, time: tracks[2].time });
    commentator = new Commentator(commentators[getRndInteger(0, commentators.length)].name, { racers: activePlayers });
    // commentator = new Commentator(commentators[0].name, race);

    socket.emit('commented', { comment: commentator.emitComment('greetings', {commentator: commentator.name, participants: activePlayers})});

    // socket.emit('commented', { comment: commentator.emitComment('presentation', activePlayers)});

    refreshIntervalId = setInterval(() => {
      socket.emit('commented', { comment: commentator.emitComment('regular')});
    }, 10000);
    // setInterval(function () {
    //   socket.broadcast.emit('commented', { comment: commentator.emitComment('regular')});
    // }, 5000);
  });

  socket.on('playerProgressed', payload => {
    const userLogin = jwt.decode(payload.token).login;
    socket.broadcast.emit('progressSubmitted', { player: userLogin });
    socket.emit('progressSubmitted', { player: userLogin });
  });

  socket.on('playerEntered', payload => {
    const userLogin = jwt.decode(payload.token).login;
    if (activePlayers.indexOf(userLogin) === -1) {
      activePlayers.push(userLogin);
    }
    socket.emit('newPlayerJoined', { players: activePlayers });
    socket.broadcast.emit('newPlayerJoined', { players: activePlayers });

    if (activePlayers.length > 0) {
      socket.emit('timeOut', { timeOut: 10 });
    }

  });

  socket.on('timeoutEnd', payload => {
    const userLogin = jwt.decode(payload.token).login;
    socket.emit('raceStarting');
  });

  socket.on('playerHasLeft', payload => {
    const userLogin = jwt.decode(payload.token).login;
    socket.broadcast.emit('playerHasLeftBroadcast', { player: userLogin });
    activePlayers.splice(activePlayers.indexOf(userLogin), 1);
  });

});

getRndInteger = function(min, max) {
  return Math.floor(Math.random() * (max - min) ) + min;
};

class Commentator {

  constructor(name, data) {
    this.name = name;
    this.data = data;
  }

  emitComment(eventType, data) {
    switch (eventType) {
      case 'greetings': {
        const { commentator, participants } = data;
        return `${comments.greetings[0]} ${commentator}. ${comments.presentation[0]} ${participants}`;
      }
      case 'regular': {
        let rnd = getRndInteger(0, comments.regular.length);
        return comments.regular[rnd];
      }
      case 'presentation': {
        return `${comments.presentation[0]} ${data}`;
      }
      default: return null;
    }

    // return new Comment(eventType, data);
  }

}

class Comment {

  constructor(type, data) {

    let comment;

    switch (type) {
      case 'almostFinished': {
        comment = new AlmostFinished()
      }

      case 'greetings': {
        comment = new Greetings()
      }

      case 'raceStarted': {
        comment = new RaceStarted()
      }

      case 'raceFinished': {
        comment = new RaceFinished()
      }

      case 'overTake': {
        comment = new OverTake()
      }

      case 'presentation': {
        comment = new Presentation()
      }

      case 'raceSummary': {
        comment = new RaceSummary()
      }

      case 'regular': {
        comment = new Regular()
      }

      default: {
        throw new Error("No type set for the comment!");
      }
    }
  }
}

// todo: add random pick one of comments

// class AlmostFinished {
//   constructor () {
//     this.body = ''
//   }
// }

class Greetings {
  constructor () {
    this.body = comments.greetings[0];
  }
}

// class OverTake {
//   constructor () {
//     this.body = ''
//   }
// }

// class Presentation {
//   constructor () {
//     this.body = ''
//   }
// }

// class RaceFinished {
//   constructor () {
//     this.body = ''
//   }
// }

// class RaceStarted {
//   constructor () {
//     this.body = ''
//   }
// }

// class RaceSummary {
//   constructor () {
//     this.body = ''
//   }
// }

class Regular {
  constructor () {
    this.body = comments.regular[0];
  }
}
