// Factory || Proxy

export default class Comment {

  constructor(type, data) {

    let comment;

    switch (type) {
      case 'almostFinished': {
        comment = new AlmostFinished()
      }

      case 'greetings': {
        comment = new Greetings()
      }

      case 'raceStarted': {
        comment = new RaceStarted()
      }

      case 'raceFinished': {
        comment = new RaceFinished()
      }

      case 'overTake': {
        comment = new OverTake()
      }

      case 'presentation': {
        comment = new Presentation()
      }

      case 'raceSummary': {
        comment = new RaceSummary()
      }

      case 'regular': {
        comment = new Regular()
      }

      default: {
        throw new Error("No type set for the comment!");
      }
    }
  }
}

class AlmostFinished {
  constructor () {
    this.body = ''
  }
}

class Greetings {
  constructor () {
    this.body = ''
  }
}

class OverTake {
  constructor () {
    this.body = ''
  }
}

class Presentation {
  constructor () {
    this.body = ''
  }
}

class RaceFinished {
  constructor () {
    this.body = ''
  }
}

class RaceStarted {
  constructor () {
    this.body = ''
  }
}

class RaceSummary {
  constructor () {
    this.body = ''
  }
}

class Regular {
  constructor () {
    this.body = ''
  }
}

// export { Comment }
